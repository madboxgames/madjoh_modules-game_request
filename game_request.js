define([
	'require',
	'madjoh_modules/ajax/ajax',
],
function(require, AJAX){
	var GameRequest = {
		send : function(friend_ids, code, data){
			var parameters = {
				friend_ids 	: JSON.stringify(friend_ids),
				code 		: code
			};

			if(data) parameters.data = JSON.stringify(data);

			return AJAX.post('/request/send', {parameters : parameters});
		},
		answer : function(request_id, accept){
			var parameters = {
				request_id 	: request_id,
				accept 		: accept
			};

			return AJAX.post('/request/answer', {parameters : parameters});
		},

		getReceived : function(){
			return AJAX.post('/request/get/received');
		},
		getSent : function(){
			return AJAX.post('/request/get/sent');
		}
	};

	return GameRequest;
});